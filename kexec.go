package main

import (
	"fmt"
	"io"
	"os"
	"os/exec"
)

func readWriteSectionPath(r io.ReaderAt, sectionOffset int64, bufferSize int64) (string, error) {
	file, err := os.CreateTemp("/tmp", "ospkg")
	if err != nil {
		return "", err
	}
	defer file.Close()
	if bufferSize == 0 {
		bufferSize = 1024
	}
	buffer := make([]byte, bufferSize)
	for {
		n, err := r.ReadAt(buffer, sectionOffset)
		if err != nil && err != io.EOF {
			return "", err
		}
		if n == 0 {
			break
		}
		_, err = file.Write(buffer[:n])
		if err != nil {
			return "", err
		}
		sectionOffset += int64(n)
	}
	info, err := file.Stat()
	if err != nil {
		return "", err
	}
	filepath := fmt.Sprintf("/tmp/%s", info.Name())
	return filepath, nil
}

func Execute(kernel, initrd io.ReaderAt, cmdline string) error {
	kernelPath, err := readWriteSectionPath(kernel, 0, 0)
	if err != nil {
		return err
	}
	initrdPath, err := readWriteSectionPath(initrd, 0, 0)
	if err != nil {
		return err
	}
	kexec := exec.Command("kexec", "-l", kernelPath, "--initrd", initrdPath, "--command-line", cmdline)
	if err := kexec.Run(); err != nil {
		return err
	}
	if err := exec.Command("systemctl", "kexec").Run(); err != nil {
		return err
	}
	return nil
}
