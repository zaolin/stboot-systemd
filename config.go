package main

import (
	"context"
	"encoding/json"
	"os"

	"system-transparency.org/stboot/host"
	"system-transparency.org/stboot/trust"
)

const (
	TrustPolicyFile = "/etc/trust_policy/trust_policy.json"
	SigningRootFile = "/etc/trust_policy/ospkg_signing_root.pem"
	HttpsRootsFile  = "/etc/trust_policy/isrgrootx1.pem"
)

func ReadHostConfig() (*host.Config, error) {
	ctx := context.Background()
	reader, err := host.ConfigAutodetect(ctx)
	if err != nil {
		return nil, err
	}
	var config host.Config
	d := json.NewDecoder(reader)
	if err := d.Decode(&config); err != nil {
		return nil, err
	}
	return &config, nil
}

func ReadTrustPolicy() (*trust.Policy, error) {
	reader, err := os.Open(TrustPolicyFile)
	if err != nil {
		return nil, err
	}
	var policy trust.Policy
	d := json.NewDecoder(reader)
	if err := d.Decode(&policy); err != nil {
		return nil, err
	}
	return &policy, nil
}
